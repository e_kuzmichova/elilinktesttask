package task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import task4.helpers.Data;

import java.io.IOException;

public class FtpTest {

    private FtpClient ftpClient;
    private static Logger log = LogManager.getLogger();

    @BeforeTest
    public void setup() throws IOException {
        log.info("Prepating FTP test.");
        ftpClient = new FtpClient();
        ftpClient.connect(Data.IP);
        System.out.println(ftpClient.readLine());
        System.out.println(ftpClient.readLine());
    }

    @AfterTest
    public void teardown() throws IOException {
        log.info("FTP test complete.");
        ftpClient.disconnect();
    }

    @Test
    public void ftpClientTest() throws IOException {
        log.info("Executing FTP commands.");
        ftpClient.pwd();
        System.out.println(ftpClient.readLine());
        ftpClient.cwd(Data.SUB_DIRECTORY_NAME);
        System.out.println(ftpClient.readLine());
        ftpClient.pwd();
        System.out.println(ftpClient.readLine());
        ftpClient.cdup();
        System.out.println(ftpClient.readLine());
        ftpClient.pwd();
        ftpClient.makeAndDeleteDirectory(Data.NEW_DIRECTORY_NAME);
        System.out.println(ftpClient.readLine());
    }
}