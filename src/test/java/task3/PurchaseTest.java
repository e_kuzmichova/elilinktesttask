package task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import task3.helpers.*;
import task3.pages.*;

import java.util.concurrent.TimeUnit;

public class PurchaseTest {

    private WebDriver driver;
    private HomePage homePage;
    private TicketsSelectionPage ticketsSelectionPage;
    private TripSummaryPage tripSummaryPage;
    private PassengerInfoPage passengerInfoPage;
    private CreditCardInfoPage creditCardInfoPage;
    private static Logger log = LogManager.getLogger();

    @BeforeTest
    public void setup() {
        log.info("Preparing Purchase test.");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(Data.URL);
        homePage = new HomePage(driver);
    }

    @AfterTest
    public void teardown() {
        log.info("Purchase test complete.");
        driver.close();
    }

    @Test
    public void completePurchaseTest() {
        log.info("Test if tickets purchase is successful.");
        homePage.selectBookATripMenu(BookATripMenu.FLIGHT);
        homePage.selectFlightType(FlightType.ROUND_TRIP);
        homePage.setFlightInfo(Data.FROM, Data.TO, Data.DEPART_DATE, Data.RETURN_DATE, DateType.EXACT_DATES, PriceType.MONEY);
        ticketsSelectionPage = homePage.findFlights();
        tripSummaryPage = ticketsSelectionPage.selectTickets();
        passengerInfoPage = tripSummaryPage.clickContinue();
        passengerInfoPage.setPassengerInfo(Data.FIRST_NAME, Data.LAST_MAME, Data.PHONE_NUMBER, Data.EMAIL, Data.AGE);
        creditCardInfoPage = passengerInfoPage.clickContinue();
        Assert.assertTrue(creditCardInfoPage.isCompletePurchaseButtonVisible(), "Complete Purchase Button is invisible");
    }
}
