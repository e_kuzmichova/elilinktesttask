package task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.Test;
import task2.helpers.CommonDataProvider;
import task2.helpers.Data;
import task2.pages.InboxPage;
import task2.pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class InboxTest {

    private static Logger log = LogManager.getLogger();
    private WebDriver driver;
    private LoginPage loginPage;
    private InboxPage inboxPage;

    @BeforeMethod
    public void setup() {
        log.info("Preparing Inbox test.");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(Data.URL);
        loginPage = new LoginPage(driver);
        inboxPage = loginPage.login(Data.USERNAME, Data.PASSWORD);
    }

    @AfterMethod
    public void teardown() {
        log.info("Inbox test complete.");
        driver.close();
    }


    @Test(dataProviderClass = CommonDataProvider.class, dataProvider = "MailFolders")
    public void openFolderTest(String title) {
        log.info("Test switching to folder.");
        inboxPage.openFolder(title);
        Assert.assertTrue(driver.getTitle().contains(title), "Error opening folder");
    }

    @Test
    public void searchTest() {
        log.info("Mail search test.");
        inboxPage.search(Data.SEARCH_TEXT);
        Assert.assertTrue(inboxPage.countResults() == Data.EXPECTED_VALUE,
                "The count if results doesn't match with expected count");
    }

    @Test
    public void isMessageSentTest() {
        log.info("Test if sending message was successful.");
        inboxPage.sentMail(Data.USERNAME, Data.SUBJECT, Data.MESSAGE);
        Assert.assertTrue(inboxPage.isMessageSent(), "Error sending message");
    }

    @Test
    public void logoutTest() {
        log.info("Test logging out.");
        loginPage = inboxPage.logout();
        Assert.assertTrue(loginPage.isLogout(), "Logout error");
    }
}
