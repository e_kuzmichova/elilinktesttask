package task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import task1.helpers.Data;
import task1.pages.HomePage;
import task1.pages.SearchResultPage;

import java.util.concurrent.TimeUnit;


public class SearchTest {

    private WebDriver driver;
    private HomePage homePage;
    private SearchResultPage searchResultPage;

    private static Logger log = LogManager.getLogger();

    @BeforeMethod
    public void setup() {
        log.debug("Preparing search test.");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(Data.URL);
        homePage = new HomePage(driver);
    }

    @AfterMethod
    public void teardown() {
        log.debug("Search test complete.");
        driver.close();
    }

    @Test(expectedExceptions = WebDriverException.class)
    public void searchTest() {
        log.debug("Starting search test.");
        searchResultPage = homePage.getHeader().search(Data.SEARCH);
        System.out.println("Number of results per page = " + searchResultPage.countSearchResults());
        searchResultPage.searchInResults(Data.SEARCH_IN_RESULTS);
    }
}
