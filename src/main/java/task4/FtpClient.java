package task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task4.helpers.Data;

import java.io.*;
import java.net.Socket;

public class FtpClient {

    private Socket socket = null;
    private BufferedReader reader = null;
    private BufferedWriter writer = null;
    private static boolean DEBUG = false;
    protected static Logger log = LogManager.getLogger();

    public void connect(String host) throws IOException {
        connect(host, 21);
    }

    public void connect(String host, int port) throws IOException {
        log.info("Trying to connect to host " + host + ":" + port);
        if (socket != null) {
            throw new IOException("Already connected");
        }
        socket = new Socket(host, port);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream()));
        log.info("Authentication");
        sendLine("USER " + Data.USERNAME);
        sendLine("PASS " + Data.PASSWORD);
    }

    public boolean cwd(String dir) throws IOException {
        log.info("Changing working directory to /" + dir);
        sendLine("CWD " + dir);
        String response = readLine();
        return (response.startsWith("250 "));
    }

    public void pwd() throws IOException {
        log.info("Print working directory.");
        sendLine("PWD");
    }

    public void cdup() throws IOException {
        log.info("Change directory up - to parent.");
        sendLine("CDUP");
    }

    private void makeDirectory(String directory) throws IOException {
        log.info("Make new directory " + directory);
        sendLine("MKD " + directory);
    }

    private void removeDirectory(String directory) throws IOException {
        log.info("Remove directory " + directory);
        sendLine("RMD " + directory);
    }

    public void makeAndDeleteDirectory(String directory) throws IOException {
        makeDirectory(directory);
        if(!readLine().contains("550 ")) {
            removeDirectory(directory);
        }
    }

    //  Sending raw FTP command to server
    private void sendLine(String line) throws IOException {
        if (socket == null) {
            throw new IOException("Not connected.");
        }
        try {
            writer.write(line + "\r\n");
            writer.flush();
            if (DEBUG) {
                System.out.println("> " + line);
            }
        } catch (IOException e) {
            socket = null;
            throw e;
        }
    }

    //  Reading FTP server response line
    public String readLine() throws IOException {
        String line = reader.readLine();
        if (DEBUG) {
            System.out.println("< " + line);
        }
        log.info("FTP response: " + line);
        return line;
    }

    public void disconnect() throws IOException {
        log.info("Disconnecting from server.");
        try {
            sendLine("QUIT\n");
        } finally {
            socket = null;
        }
    }
}
