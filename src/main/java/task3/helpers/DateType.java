package task3.helpers;

import org.openqa.selenium.By;

public enum DateType {

    EXACT_DATES(Locators.EXACT_DATES), FLAXIBLE_DAYS(Locators.FLAXIBLE_DAYS);

    private By locator;

    DateType(By locator) {
        this.locator = locator;
    }

    public By getLocator() {
        return locator;
    }
}
