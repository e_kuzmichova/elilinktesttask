package task3.helpers;

import org.openqa.selenium.By;

public interface Locators {

    //BookATripMenu    Locators.
    By FLIGHT = By.id("book-air-content-trigger");
    By HOTEL = By.id("book-hotel-content-trigger");
    By CAR = By.id("book-car-content-trigger");
    By VOCATION_PACKAGES = By.id("book-delta-vacations-content-trigger");

    //DateType
    By EXACT_DATES = By.id("exactDaysBtn");
    By FLAXIBLE_DAYS = By.id("flexDaysBtn");

    //FlightType
    By ROUND_TRIP = By.id("roundTripBtn");
    By ONE_WAY = By.id("oneWayBtn");
    By MULTI_CITY = By.id("air-shopping-multicity-link");

    //NavigationMenu
    By MY_TRIPS = By.id("navMyTrips");
    By BOOK_A_TRIP = By.id("navBookTrip");
    By FLIGHT_STATUS = By.id("navFlightStatus");
    By CHECK_IN = By.id("navCheckIn");

    //PriceType
    By MONEY = By.id("cashBtn");
    By MILES = By.id("milesBtn");

    //HomePage
    By FROM_INPUT = By.id("originCity");
    By TO_INPUT = By.id("destinationCity");
    By DEPART_DATE_INPUT = By.id("departureDate");
    By RETURN_DATE_INPUT = By.id("returnDate");
    By FIND_FLIGHTS_BUTTON = By.id("findFlightsSubmit");

    //PassengerInfoPage
    By FIRST_NAME_INPUT = By.id("firstName0");
    By LAST_NAME_INPUT = By.id("lastName0");
    By GENDER_BUTTON = By.id("gender0-button");
    By DATE_OF_BIRTH_MONTH_BUTTON = By.id("month0-button");
    By DATE_OF_BIRTH_DAY_BUTTON = By.id("day0-button");
    By DATE_OF_BIRTH_YEAR_BUTTON = By.id("year0-button");
    By DECLINE_CONTACT_NO_BUTTON = By.cssSelector("[for='declineContactN_0']");
    By PHONE_NUMBER_INPUT = By.id("telephoneNumber0");
    By EMAIL_INPUT = By.id("email");
    By CONFIRM_EMAIL_INPUT = By.id("reEmail");
    By PI_CONTINUE_BUTTON = By.id("paxReviewPurchaseBtn");

    //TicketsSelectionPage
    By SELECT_FIRST_TICKET_BUTTON = By.cssSelector("[id='0_0_0']");

    //TripSummaryPage
    By TS_CONTINUE_BUTTON = By.id("tripSummarySubmitBtn");

    //CreditCardInfoPage
    By COMPLETE_PURCHASE_BUTTON = By.id("continue_button");
}
