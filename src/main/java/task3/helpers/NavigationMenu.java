package task3.helpers;

import org.openqa.selenium.By;

public enum NavigationMenu {

    MY_TRIPS(Locators.MY_TRIPS), BOOK_A_TRIP(Locators.BOOK_A_TRIP),
    FLIGHT_STATUS(Locators.FLIGHT_STATUS), CHECK_IN(Locators.CHECK_IN);

    private By locator;

    NavigationMenu(By locator) {
        this.locator = locator;
    }

    public By getLocator() {
        return locator;
    }
}
