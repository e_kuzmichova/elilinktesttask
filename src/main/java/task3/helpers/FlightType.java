package task3.helpers;

import org.openqa.selenium.By;

public enum FlightType {

    ROUND_TRIP(Locators.ROUND_TRIP), ONE_WAY(Locators.ONE_WAY),
    MULTI_CITY(Locators.MULTI_CITY);

    private By locator;

    FlightType(By locator) {
        this.locator = locator;
    }

    public By getLocator() {
        return locator;
    }
}
