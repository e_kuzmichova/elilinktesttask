package task3.helpers;

import org.openqa.selenium.By;

public enum BookATripMenu {

    FLIGHT(Locators.FLIGHT), HOTEL(Locators.HOTEL),
    CAR(Locators.CAR), VOCATION_PACKAGES(Locators.VOCATION_PACKAGES);

    private By locator;

    BookATripMenu(By locator) {
        this.locator = locator;
    }

    public By getLocator() {
        return locator;
    }
}
