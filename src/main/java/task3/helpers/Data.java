package task3.helpers;

public interface Data {
    String URL = "https://www.delta.com/";
    String FROM = "JFK";
    String TO = "SVO";
    String DEPART_DATE = "12/01/2016";
    String RETURN_DATE = "12/05/2016";
    String FIRST_NAME = "Kate";
    String LAST_MAME = "Kate";
    String PHONE_NUMBER = "1234567";
    String EMAIL = "qwerty@gmail.com";
    int AGE = 25;
}
