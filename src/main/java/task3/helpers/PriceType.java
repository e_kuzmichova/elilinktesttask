package task3.helpers;

import org.openqa.selenium.By;

public enum PriceType {

    MONEY(Locators.MONEY), MILES(Locators.MILES);

    private By locator;

    PriceType(By locator) {
        this.locator = locator;
    }

    public By getLocator() {
        return locator;
    }
}
