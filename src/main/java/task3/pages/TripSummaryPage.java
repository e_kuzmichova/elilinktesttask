package task3.pages;

import org.openqa.selenium.WebDriver;
import task3.helpers.Locators;

public class TripSummaryPage extends BasePage {

    public TripSummaryPage(WebDriver driver) {
        super(driver);
    }

    public PassengerInfoPage clickContinue() {
        log.info("Going to the Passenger Info page.");
        getDriver().findElement(Locators.TS_CONTINUE_BUTTON).click();
        return new PassengerInfoPage(getDriver());
    }
}
