package task3.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class BasePage {
    private WebDriver driver;
    protected static Logger log = LogManager.getLogger();

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void selectFirstInDropdown() {
        log.info("Select first element in dropdown list.");
        new Actions(getDriver()).sendKeys(Keys.DOWN).build().perform();
        new Actions(getDriver()).sendKeys(Keys.ENTER).build().perform();
    }

    public boolean isElementDisplayed(By element) {
        log.info("Checking if element " + element + " is visible.");
        try {
            return driver.findElement(element).isDisplayed();
        } catch (NoSuchElementException e) {
            log.info("Element " + element + " not found.");
            return false;
        }
    }
}
