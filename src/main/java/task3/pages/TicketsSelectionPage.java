package task3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import task3.helpers.Locators;

public class TicketsSelectionPage extends BasePage {

    public TicketsSelectionPage(WebDriver driver) {
        super(driver);
    }

    private void selectFirstTicket() {
        log.info("Selecting first ticket on the page.");
        getDriver().findElement(Locators.SELECT_FIRST_TICKET_BUTTON).click();
    }

    public TripSummaryPage selectTickets() {
        selectFirstTicket();
        new WebDriverWait(getDriver(), 10)
                .until(ExpectedConditions.visibilityOfElementLocated(Locators.SELECT_FIRST_TICKET_BUTTON));
        selectFirstTicket();
        log.info("Both tickets selected.");
        return new TripSummaryPage(getDriver());
    }
}
