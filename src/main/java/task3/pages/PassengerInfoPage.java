package task3.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import task3.helpers.Locators;

public class PassengerInfoPage extends BasePage {

    public PassengerInfoPage(WebDriver driver) {
        super(driver);
    }

    public void setPassengerInfo(String firstName, String lastName, String phone, String email, int age) {
        log.info("Filling out passenger info page.");
        getDriver().findElement(Locators.FIRST_NAME_INPUT).sendKeys(firstName);
        log.info("First name entered.");
        getDriver().findElement(Locators.LAST_NAME_INPUT).sendKeys(lastName);
        log.info("Last name entered.");
        getDriver().findElement(Locators.GENDER_BUTTON).click();
        selectFirstInDropdown();
        log.info("Male gender specified.");
        getDriver().findElement(Locators.DATE_OF_BIRTH_MONTH_BUTTON).click();
        selectFirstInDropdown();
        getDriver().findElement(Locators.DATE_OF_BIRTH_DAY_BUTTON).click();
        selectFirstInDropdown();
        getDriver().findElement(Locators.DATE_OF_BIRTH_YEAR_BUTTON).click();
        setYear(age);
        log.info("Date of birth specified.");
        getDriver().findElement(Locators.DECLINE_CONTACT_NO_BUTTON).click();
        log.info("Emergency contact info refused.");
        getDriver().findElement(Locators.PHONE_NUMBER_INPUT).sendKeys(phone);
        log.info("Phone number entered.");
        getDriver().findElement(Locators.EMAIL_INPUT).sendKeys(email);
        log.info("Email address entered.");
        getDriver().findElement(Locators.CONFIRM_EMAIL_INPUT).sendKeys(email);
        log.info("Email address confirmed.");
    }

    private void setYear(int age) {
        for(int i = 0; i <= age; i++) {
            new Actions(getDriver()).sendKeys(Keys.DOWN).build().perform();
        }
        new Actions(getDriver()).sendKeys(Keys.ENTER).build().perform();
    }

    public CreditCardInfoPage clickContinue() {
        log.info("Going to Credit Card info page.");
        getDriver().findElement(Locators.PI_CONTINUE_BUTTON).click();
        return new CreditCardInfoPage(getDriver());
    }
}
