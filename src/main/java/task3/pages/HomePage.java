package task3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import task3.helpers.*;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void selectTabMenu(NavigationMenu tab) {
        log.info("Switching to " + tab + " tab.");
        getDriver().findElement(tab.getLocator()).click();
    }

    public void selectBookATripMenu(BookATripMenu tab) {
        log.info("Switching to " + tab + " tab.");
        getDriver().findElement(tab.getLocator()).click();
    }

    public void selectFlightType(FlightType flightType) {
        log.info("Selecting flight type.");
        getDriver().findElement(flightType.getLocator()).click();
    }

    private void enterAirports(String from, String to) {
        log.info("Selecting From and To airports.");
        getDriver().findElement(Locators.FROM_INPUT).sendKeys(from);
        getDriver().findElement(Locators.TO_INPUT).sendKeys(to);
    }

    private void selectDepartureDate(String departDate) {
        log.info("Selecting departure date.");
        getDriver().findElement(Locators.DEPART_DATE_INPUT).sendKeys(departDate);
    }

    private void selectArrivalDate(String returnDate) {
        log.info("Selecting arrival date.");
        getDriver().findElement(Locators.RETURN_DATE_INPUT).sendKeys(returnDate);
    }

    private void selectDateType(DateType dateType) {
        log.info("Selecting date type.");
        getDriver().findElement(dateType.getLocator()).click();
    }

    private void selectPriceType(PriceType priceType) {
        log.info("Selecting price type.");
        getDriver().findElement(priceType.getLocator()).click();
    }

    public TicketsSelectionPage findFlights() {
        log.info("Searching for flights.");
        getDriver().findElement(Locators.FIND_FLIGHTS_BUTTON).click();
        log.info("Waiting for search results to load.");
        new WebDriverWait(getDriver(), 10)
                .until(ExpectedConditions.visibilityOfElementLocated(Locators.SELECT_FIRST_TICKET_BUTTON));
        log.info("Going to tickets selection page.");
        return new TicketsSelectionPage(getDriver());
    }

    public void setFlightInfo(String from, String to, String departDate, String returnDate, DateType dateType,PriceType priceType) {
        log.info("Filling out flight information.");
        enterAirports(from, to);
        selectDepartureDate(departDate);
        selectArrivalDate(returnDate);
        selectDateType(dateType);
        selectPriceType(priceType);
    }
}
