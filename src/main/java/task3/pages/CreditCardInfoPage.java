package task3.pages;

import org.openqa.selenium.WebDriver;
import task3.helpers.Locators;

public class CreditCardInfoPage extends BasePage {

    public CreditCardInfoPage(WebDriver driver) {
        super(driver);
    }

    public boolean isCompletePurchaseButtonVisible() {
        log.info("Checking if 'Complete purchase' button is visible.");
        return isElementDisplayed(Locators.COMPLETE_PURCHASE_BUTTON);
    }
}
