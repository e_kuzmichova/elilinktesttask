package task1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import task1.helpers.Locators;

public class SearchResultPage extends BasePage {

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public int countSearchResults() {
        log.info("Counting the number of results on the page.");
        return getDriver().findElements(Locators.SEARCH_RESULTS).size();
    }

    public void searchInResults(String name) {
        log.info("Searching certain result among all results on the page.");
        By locator = By.xpath(String.format("//li/h3/a[2][text()[contains(.,'%s')]]", name));

        if(isElementDisplayed(locator)) {
            log.info("Sought-for result found. Going over to its page.");
            getDriver().findElement(locator).click();
        } else {
            log.info("Sought-for result not found.");
            throw new WebDriverException();
        }
    }
}
