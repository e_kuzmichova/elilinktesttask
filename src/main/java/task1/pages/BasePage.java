package task1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task1.pages.parts.Header;

public class BasePage {

    protected static Logger log = LogManager.getLogger();
    private WebDriver driver;
    private Header header;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.header = new Header(driver);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public Header getHeader() {
        return header;
    }

    public boolean isElementDisplayed(By element) {
        log.info("Checking if element " + element + " is visible.");
        try {
            return driver.findElement(element).isDisplayed();
        } catch (NoSuchElementException e) {
            log.info("Element " + element + " not found.");
            return false;
        }
    }
}
