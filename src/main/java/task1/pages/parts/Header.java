package task1.pages.parts;

import org.openqa.selenium.WebDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task1.helpers.Locators;
import task1.pages.SearchResultPage;

public class Header {

    private WebDriver driver;
    static Logger log = LogManager.getLogger();

    public Header(WebDriver driver) {
        this.driver = driver;
    }

    public SearchResultPage search(String search) {
        log.info("Search initiated.");
        driver.findElement(Locators.SEARCH_INPUT).sendKeys(search);
        driver.findElement(Locators.SEARCH_BUTTON).click();
        log.info("Going to the search results page.");
        return new SearchResultPage(driver);
    }
}
