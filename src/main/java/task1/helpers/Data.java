package task1.helpers;

public interface Data {

    String URL = "https://www.tut.by/";
    String SEARCH = "automated testing";
    String SEARCH_IN_RESULTS = "Minsk Automated Testing Community";
}
