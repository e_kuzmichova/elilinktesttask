package task1.helpers;

import org.openqa.selenium.By;

public interface Locators {

    //Header
    By SEARCH_INPUT = By.id("search_from_str");
    By SEARCH_BUTTON = By.cssSelector(".button.big");

    //SearchResultPage
    By SEARCH_RESULTS = By.cssSelector(".b-results__li>h3");
}
