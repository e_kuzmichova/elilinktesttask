package task2.helpers;

import org.openqa.selenium.By;

public interface Locators {

    //LoginPage
    By EMAIL_INPUT = By.id("Email");
    By NEXT_BUTTON = By.id("next");
    By PASSWORD_INPUT = By.id("Passwd");
    By LOGIN_BUTTON = By.id("signIn");

    //InboxPage
    By MORE = By.className("CJ");
    By SEARCH_INPUT = By.id("gbqfq");
    By SEARCH_BUTTON = By.id("gbqfb");
    By COMPOSE_BUTTON = By.cssSelector(".z0>div");
    By TO_INPUT = By.cssSelector(".wO.nr.l1>textarea");
    By SUBJECT_INPUT = By.cssSelector("[name='subjectbox']");
    By MESSAGE_INPUT = By.cssSelector("div[aria-label='Message Body']");
    By SENT_BUTTON = By.cssSelector("[role='button'][data-tooltip='Send \u202A(Ctrl-Enter)\u202C']");
    By ACCOUNT_BUTTON = By.cssSelector(".gb_b.gb_db.gb_R");
    By LOGOUT_BUTTON = By.id("gb_71");
    By SENT_MAIL_FOLDER = By.xpath("//a[@class='J-Ke n0'][@title=contains(text(), 'Sent Mail')]");
    By NO_RESULTS_MESSAGE = By.className("TC");
    By ALL_MESSAGES = By.xpath("//div[@gh='tl']//colgroup/../tbody/tr");
    String FOLDER_LINK = "//a[@class='J-Ke n0'][@title=contains(text(), '%s')]";
    By VIEW_SENT_MESSAGE_LINK = By.id("link_vsm");
}
