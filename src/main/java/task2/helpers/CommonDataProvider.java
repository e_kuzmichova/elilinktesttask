package task2.helpers;

import org.testng.annotations.DataProvider;

public class CommonDataProvider {

    @DataProvider(name = "MailFolders")
    public static Object[][] mailFoldersDataProvider() {

        return new Object[][]{
                new Object[]{"Inbox"},
                new Object[]{"Starred"},
                new Object[]{"Sent Mail"},
                new Object[]{"Drafts"},
                new Object[]{"Important"},
                new Object[]{"Chats"},
                new Object[]{"All Mail"},
                new Object[]{"Spam"},
                new Object[]{"Trash"}
        };
    }
}
