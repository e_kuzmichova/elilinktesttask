package task2.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class BasePage {

    protected static Logger log = LogManager.getLogger();
    private WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected boolean isElementDisplayed(By element) {
        log.info("Checking if element " + element + " is visible.");
        try {
            return driver.findElement(element).isDisplayed();
        } catch (NoSuchElementException e) {
            log.info("Element " + element + " not found.");
            return false;
        }
    }
}
