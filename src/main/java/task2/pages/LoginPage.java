package task2.pages;

import org.openqa.selenium.WebDriver;
import task2.helpers.Locators;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public InboxPage login(String username, String password) {
        getDriver().findElement(Locators.EMAIL_INPUT).sendKeys(username);
        log.info("Username entered.");
        getDriver().findElement(Locators.NEXT_BUTTON).click();
        log.info("Next button clicked.");
        getDriver().findElement(Locators.PASSWORD_INPUT).sendKeys(password);
        log.info("Password entered.");
        getDriver().findElement(Locators.LOGIN_BUTTON).click();
        log.info("Login button clicked.\nGoing to inbox page.");
        return new InboxPage(getDriver());
    }

    public boolean isLogout() {
        log.info("Checking if logout was successful.");
        return getDriver().findElement(Locators.LOGIN_BUTTON).isDisplayed();
    }
}
