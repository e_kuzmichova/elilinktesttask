package task2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import task2.helpers.Locators;

public class InboxPage extends BasePage {

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    private void viewMore() {
        log.info("Expanding the list of folders.");
        Actions actions = new Actions(getDriver());
        actions.moveToElement(getDriver().findElement(Locators.SENT_MAIL_FOLDER)).build().perform();
        getDriver().findElement(Locators.MORE).click();
    }

    public void openFolder(String title) {
        log.info("Opening folder " + title + ".");
        viewMore();
        String section = String.format(Locators.FOLDER_LINK, title);
        getDriver().findElement(By.xpath(section)).click();
        log.info("Awaiting the title to change (switching to the folder " + title + ").");
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.titleContains(title));
    }

    public void search(String text) {
        log.info("Search mail for a pattern.");
        getDriver().findElement(Locators.SEARCH_INPUT).sendKeys(text);
        getDriver().findElement(Locators.SEARCH_BUTTON).click();
    }

    public int countResults() {
        log.info("Counting the number of search results.");
        int count = 0;
        try {
            if(isElementDisplayed(Locators.NO_RESULTS_MESSAGE)) {
                log.info("No results found.");
                count = 0;
            }
        } catch (NoSuchElementException e) {
            count = getDriver().findElements(Locators.ALL_MESSAGES).size();
            log.info("Found " + count + " results.");
        }
        return count;
    }

    public void sentMail(String to, String subject, String message) {
        getDriver().findElement(Locators.COMPOSE_BUTTON).click();
        log.info("Compose button clicked");
        getDriver().findElement(Locators.TO_INPUT).sendKeys(to);
        log.info("Recipient specified.");
        getDriver().findElement(Locators.SUBJECT_INPUT).sendKeys(subject);
        log.info("Subject specified.");
        getDriver().findElement(Locators.MESSAGE_INPUT).sendKeys(message);
        log.info("Message body entered.");
        getDriver().findElement(Locators.SENT_BUTTON).click();
        log.info("Send message button clicked.");
    }

    public boolean isMessageSent() {
        log.info("Checking if the notification about successful sending is shown.");
        return isElementDisplayed(Locators.VIEW_SENT_MESSAGE_LINK);
    }

    public LoginPage logout() {
        log.info("Logging out of gmail.");
        getDriver().findElement(Locators.ACCOUNT_BUTTON).click();
        getDriver().findElement(Locators.LOGOUT_BUTTON).click();
        log.info("Returning to login page.");
        return new LoginPage(getDriver());
    }
}
